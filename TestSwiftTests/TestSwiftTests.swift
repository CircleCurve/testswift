//
//  TestSwiftTests.swift
//  TestSwiftTests
//
//  Created by Patrick Ng on 5/3/2018.
//  Copyright © 2018 Gameone. All rights reserved.
//

import XCTest
@testable import TestSwift

class TestSwiftTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testSwiftSyntax() {
        print("Hello World")
        let value = 3
        let value2 = 4
        
        let explicitDouble : Double  = 70
        let explicitFloat : Float = 4
        
        print("Value : ", value , "Value2 : " ,value2, "explicitDouble :", explicitDouble, "explicitFloat :", explicitFloat)
        
        let label = "The width is "
        let width = 94
        let widthLabel = label + String(width)
        
        print (widthLabel)
        
        let apples = 3
        let oranges = 5
        let appleSummary =  "I have \(apples) apples"
        let fruitSummary = "I have \(apples + oranges) fruit"
        
        print(appleSummary)
        print(fruitSummary)
        
        let quoation = """
            I said "I have \(apples) apples"
            And then I said "I have \(apples + oranges) fruit"
        """
        print(quoation)
        
        var shoppingList = ["catfish", "water", "tulips", "blue planet"]
        shoppingList[1] = "bottle of water"
        
        var occupation = [
            "Malcom" : "Captain",
            "Kaylee" : "Mechanic",
        ]
        
        occupation["Jayne"] = "Public Relations"
        
        print(shoppingList)
        print(occupation)
        
        let emptyArray = [String]()
        var emptyDictionary = [String: Float]()
        
        //emptyArra
        emptyDictionary["test"] = 2
        emptyDictionary["test1"] = 3
        
        print(emptyArray)
        print(emptyDictionary)
        
        shoppingList = []
        occupation = [:]

    }
    
    func testSwiftLoop(){
        let individualScores = [75, 43, 103, 87, 12]
        var teamScore = 0
        for score in individualScores{
            if score > 50 {
                teamScore += 3
            }else{
                teamScore += 1
            }
            
        }
        print(teamScore)
    }
    
    func testSwiftOptional () {
        var optionalString : String? = "Hello"
        print (optionalString == nil)
        
        var optionalName: String? = "John Appleseed"
        var greeting = "Hello!"
        
        if let name = optionalName {
            greeting = "Hello, \(name)"
        }
        print(greeting)
        
        let nickName : String? = nil
        let fullName : String = "John Appleseed"
        let informalGreeting = "Hi \(nickName ?? fullName)"
        print(informalGreeting)
        
        
        let vegetable = "red pepper"
        switch vegetable {
        case "celery" :
             print("Add some raisins and make ants on a log.")
        case "cucumber", "watercress":
            print("That would make a good tea sandwich.")
        case let x where x.hasSuffix("pepper") :
            print("Is it a spicy \(x)?")
            
        default :
            print("Everything tastes good in soup")
        }
    }
    
    func testSwiftForLoop () {
        let interestingNumbers = [
            "Prime" : [2, 3, 5, 7, 11, 13],
            "Fibonacci" : [1,1,2,3,5,8] ,
            "Square" : [1, 4,9,16,25] ,
        ]
        var largest:[String: Any] = [
            "kind" : "" ,
            "number" : 0
        ]
        for (kind, numbers) in interestingNumbers{
            for number in numbers {
                if number > largest["number"] as! Int {
                    largest["number"] = number
                    largest["kind"] = kind
                }
            }
        }
        print ("Largest:", largest)
        
        var n = 2
        while n < 100 {
            n += 2 
        }
        print(n)
        
        var m = 2
        repeat {
            m *= 2
        }while m < 100
        
        print(m)
        
        var total = 0
        for i in 0..<4 {
            total += i
        }
        print("total:", total)
    }
    
    func testSwiftFunction() {
        print(greet(person : "Bob", day : "Tuesday"))
        print(greet("Ruby", on : "Wednesday"))
    }
    
    func greet(person: String, day: String) -> String {
        return "Hello \(person), today is \(day)."
    }
    
    func greet(_ person:String, on day:String) ->String {
        return "Hello \(person), today is \(day)"
    }
    
    
    func testSwiftFP () {
        let statistic = calculatStatistic(scores: [5,3,100,3,9])
        print(statistic.sum)
        print(statistic.2)
        
        print(returnFifteen())
        
        let increment = makeIncrementer() ;
        print(increment(7))
        
        let numbers = [20, 19, 7, 12, 25 ]
        print ("Less than ten : " , hasAnyMatches(list: numbers, condition: lessThanTen))
        
        let numbers3 = numbers.map({(number : Int) ->Int in
            if number % 2 == 0 {
                return 0
            }
            return number * 3
        })
        print("3 times of numbers" , numbers3)
        
        let mappedNumbers = numbers.map( { number in number * 4 })
        print("4 times of numbers", mappedNumbers)
        
        let sortedNumbers = numbers.sorted { $0 > $1 }
        print("Sorted Numbers :" , sortedNumbers)
    }
    
    func testClassAndObj () {
        let shape = Shape()
        shape.numberOfSides = 7
        let shapeDescription = shape.simpleDescription()
        print(shapeDescription)
        
        let square = Square(sideLength: 5.2, name: "my test square")
        print("square area : ", square.area())
        print(square.simpleDescription())
        
        let circle = Circle(name : "my test circle", radius: 5.2)
        print("circle area :" , circle.area())
        print(circle.simpleDescription())
        
        let triangle = EquilateralTriangle(sideLength : 3.1, name: "a triangle")
        print(triangle.perimeter)
        triangle.perimeter = 9.9
        print(triangle.sideLength)
        
        let triangleAndSquare = TriangleAndSquare(size: 10, name: "another test shape")
        print("another test shape side length: " ,  triangleAndSquare.triangle.sideLength)
        print("another test shape side length: " , triangleAndSquare.square.sideLength)
        triangleAndSquare.square = Square(sideLength: 5.2, name: "my large shape")
        print("my large shape: " , triangleAndSquare.triangle.sideLength)
        print("my large shape: " , triangleAndSquare.square.sideLength)
        
        let optionalSquare : Square? = Square(sideLength: 2.5, name: "optional square")
        //let optionalSquare : Square? = nil
        let sideLength:Double? = optionalSquare?.sideLength
        print("Optional Square side length : " , sideLength ?? 2.5)
    }
    
    enum Rank : Int {
        case ace = 1
        case two, three, four, five, six, seven, eight, nine, ten
        case jack, queen, king
        
        func simpleDescription() -> String {
            switch self{
            case .ace:
                return "ace"
            case .jack:
                return "jack"
            case .queen:
                return "queen"
            case .king:
                return "king"
            default:
                return String(self.rawValue)
            }
        }
    }
    
    enum Suit : Int {
        case spades, hearts, diamonds, clubs
        func simpleDescription() -> String {
            switch self {
            case .spades:
                return "spades"
            case .hearts:
                return "hearts"
            case .diamonds:
                return "diamonds"
            case .clubs:
                return "clubs"
            }
        }
        
        func color() -> String {
            switch self {
            case .spades, .clubs:
                return "black"
            case .hearts, .diamonds:
                return "red"
                
            }
        }
    }
    
    enum ServerResponse {
        case dayLength(String, String)
        case weather(Double, Double, Double)
        case failure(String)
    }
    
    struct Card {
        var rank : Rank
        var suit : Suit
        func simpleDescription() -> String {
            return "The \(rank.simpleDescription()) of \(suit.simpleDescription())"
        }
        func fullDeckOfCard() -> [Card] {

            var n = 1
            var deck = [Card]()
            while let rank = Rank(rawValue : n) {
                var m = 1
                while let suit = Suit(rawValue : m){
                    deck.append(Card(rank: rank, suit: suit))
                    m += 1
                }
                n += 1
            }
            return deck

        }
    }
    
    func testSwiftEnum() {

        let ace  = Rank.ace
        let two  = Rank.two
        print (ace.rawValue)

        if compareTwoEnumValue(value1 : ace, value2: two) == true {
            print("Value1 is bigger than value 2")
        }else{
            print ("Value1 is less than value 2")
        }
        
        if let convertedRank = Rank(rawValue:3) {
            let threeDescription = convertedRank.simpleDescription()
            print("threeDescription :  \(threeDescription)")
        }
        
        let hearts = Suit.hearts
        let heartsDescription = hearts.simpleDescription()
        let heartsColor = hearts.color()
        print(heartsDescription)
        print(heartsColor)
        
        var success = ServerResponse.dayLength("6:00 am", "8:09 pm")
        success = ServerResponse.weather(20.0, 30.0, 15.0)
        let failure = ServerResponse.failure("Out of cheese")
        
        func getResponse(_ success : ServerResponse) {
            switch success {
            case let .dayLength(sunrise, sunset):
                print("Sunrise is at \(sunrise) and sunset is \(sunset)")
            case let .weather(minTemp, maxTemp, humidity):
                print("The minimum temperature is \(minTemp), max temperature is \(maxTemp) and humidity is \(humidity)")
            case let .failure(message):
                print("Failure...\(message)")
            }
        }
        getResponse(success)

        
        let threeOfSpades = Card(rank: .three, suit: .spades)
        print("Three of spades , ", threeOfSpades.simpleDescription())
        let card = Card(rank: Rank.ace, suit: Suit.clubs)
        let deck = card.fullDeckOfCard()
        print(deck)
        
    }
    
    func testSwifeProtocol () {
        let a = SimpleClass()
        a.adjust()
        let aDescription = a.simpleDescription
        print(aDescription)
        
        var b = SimpleStructure()
        b.adjust()
        let bDescription = b.simpleDescription
        print(bDescription)
        
        var c = SimpleEnumeration.Base
        c.adjust()
        let cDescription = c.simpleDescription
        print(cDescription)
        
        var number = 7
        number.adjust()
        print(number.simpleDescription)
        
        var negativeNumber : Double = -1.0
        negativeNumber.adjust()
        print(negativeNumber.absoluteValue)
        
        let protocolValue : ExampleProtocols = a
        print(protocolValue.simpleDescription)
    }
    
    func testSwiftErrorHandling () {
        let printer = Printer()
        do {
            //let printerResponse = try printer.send( job : 1, toPrinter : "Never has toner")
            let printerResponse = try printer.send( job : 1, toPrinter : "on fire")
            //let printerResponse = try printer.send( job : 1, toPrinter : "Gutenberg")
            print(printerResponse)
        } catch PrinterError.onFire {
            print("I'll just put this over here, with the rest of the fire.")
        } catch let printerError as PrinterError {
            print("Printer error :\(printerError).")
        } catch {
            print(error)
        }
        
        let printerSuccess = try? printer.send(job: 1, toPrinter: "Mergenthaler")
        let printerFailure = try? printer.send( job : 1, toPrinter : "Never has toner")
        
        print(printerSuccess ?? "unknown")
        print(printerFailure ?? "unknown")
        
    }
    
    func testSwiftDefer() {
        fridgeContains("banna")
        print(fridgeIsOpen)
    }
    
    enum OptionValue<Wrapped> {
        case none
        case some(Wrapped)
    }
    
    func testSwiftGeneric() {
        print(makeArray(repeating: "knock", numberOfTimes: 4))
        
        var possibleInteger : OptionValue<Int> = .none
        possibleInteger = .some(100)
        
        switch possibleInteger {
        case let .some(Wrapped) :
            print("wrapped value : \(Wrapped)")
        case .none:
            print("none")
        }
        
        print("Any component,",anyCommonElements([1, 2, 3], [3]))

    }
    
    func compareTwoEnumValue (value1 : Rank, value2 : Rank) -> Bool {
        let value1RawValue : Int = value1.rawValue
        let value2RawValue : Int = value2.rawValue
        if value1RawValue > value2RawValue {
            return true
        }
        return false
    }
    
    func calculatStatistic(scores:[Int]) -> (min:Int, max:Int, sum:Int){
        var min = scores[0]
        var max = scores[0]
        var sum = 0
        
        for score in scores {
            if score > max {
                max = score
            }else if score < min {
                min = score
            }
            sum += score
        }
        return (min, max, sum)
    }
    
    func returnFifteen() -> Int {
        var y = 10
        func add() {
            y += 5
        }
        add ()
        return y
    }
    
    func makeIncrementer() -> ((Int) -> Int ) {
        func addOne (number : Int ) -> Int {
            return 1 + number
        }
        return addOne
    }
    
    func lessThanTen(number : Int) -> Bool {
        return number < 10
    }
    
    func hasAnyMatches(list:[Int], condition:(Int)->Bool)->Bool {
        for item in list{
            if condition(item) {
                return true
            }
        }
        return false
    }
    
    var fridgeIsOpen = false
    let fridgeContent = ["milk", "eggs", "leftovers"]
    
    func fridgeContains(_ food: String) -> Bool {
        fridgeIsOpen = true
        defer {
            fridgeIsOpen = false
        }
        
        let result = fridgeContent.contains(food)
        return result
    }
    //=====Generic
    
    func makeArray<Item>(repeating item:Item, numberOfTimes : Int) -> [Item] {
        var result = [Item]()
        for _ in 0..<numberOfTimes {
            result.append(item)
        }
        return result
    }
    
    func anyCommonElements<T: Sequence, U: Sequence>(_ lhs: T, _ rhs: U) -> [Any]
        where T.Iterator.Element: Equatable, T.Iterator.Element == U.Iterator.Element {
            var equal = [Any]()
            for lhsItem in lhs {
                for rhsItem in rhs {
                    if lhsItem == rhsItem {
                        equal.append(lhsItem)
                    }
                }
            }
            return equal
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
