//
//  PrintError.swift
//  TestSwift
//
//  Created by Patrick Ng on 7/3/2018.
//  Copyright © 2018 Gameone. All rights reserved.
//

import Foundation
enum PrinterError : Error {
    case outOfPaper
    case noToner
    case onFire
}
