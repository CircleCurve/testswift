//
//  Printer.swift
//  TestSwift
//
//  Created by Patrick Ng on 7/3/2018.
//  Copyright © 2018 Gameone. All rights reserved.
//

import Foundation
class Printer {
    func send(job: Int, toPrinter printerName: String) throws -> String {
        switch printerName {
        case "Never has toner":
            throw PrinterError.noToner
        case "on fire":
            throw PrinterError.onFire
        case "out of paper":
            throw PrinterError.outOfPaper
        default:
            return "Job sent"
        }
    }
}
