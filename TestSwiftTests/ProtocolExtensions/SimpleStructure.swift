//
//  SimpleStructure.swift
//  TestSwift
//
//  Created by Patrick Ng on 7/3/2018.
//  Copyright © 2018 Gameone. All rights reserved.
//

import Foundation
struct SimpleStructure : ExampleProtocols {
    var simpleDescription: String = "A simple structure"
    mutating func adjust() {
        simpleDescription += "(adjust)"
    }
}
