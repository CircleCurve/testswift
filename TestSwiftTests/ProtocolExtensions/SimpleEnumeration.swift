//
//  SimpleEnumeration.swift
//  TestSwift
//
//  Created by Patrick Ng on 7/3/2018.
//  Copyright © 2018 Gameone. All rights reserved.
//

import Foundation
enum SimpleEnumeration : ExampleProtocols  {
    case Base, Adjusted
    var simpleDescription: String {
        return self.getDescription()
    }
    
    func getDescription () -> String {
        switch self {
        case .Base :
            return "A simple description of enum"
        case .Adjusted :
            return "Adjusted description of enum"
        }
        
    }
    
    mutating func adjust() {
        self = SimpleEnumeration.Adjusted
    }
}
