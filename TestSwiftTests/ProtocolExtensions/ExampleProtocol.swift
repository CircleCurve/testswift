//
//  ExampleProtocal.swift
//  TestSwift
//
//  Created by Patrick Ng on 7/3/2018.
//  Copyright © 2018 Gameone. All rights reserved.
//

import Foundation
protocol ExampleProtocols {
    var simpleDescription : String { get }
    mutating func adjust()
}

extension Int : ExampleProtocols {
    /*
    var simpleDescription : String {
        get {
            return "The number \(self)"
        }
    }*/
    var simpleDescription : String {
        return "The number \(self)"
    }
    
    mutating func adjust() {
        self += 42
    }
}

extension Double : ExampleProtocols{
    
    var simpleDescription: String {
        return "The number \(self)"
    }

    var absoluteValue : String {
        return "The absolute value \(self)"
    }
    
    mutating func adjust() {
        if self < 0 {
            self *= -1
        }
    }
}
