//
//  SimpleClass.swift
//  TestSwift
//
//  Created by Patrick Ng on 7/3/2018.
//  Copyright © 2018 Gameone. All rights reserved.
//

import Foundation
class SimpleClass : ExampleProtocols {
    var simpleDescription: String = "A very simple class"
    var anotherProperty: Int = 69105

    func adjust() {
        simpleDescription += "Now 100% adjust"
    }
    
    
}
