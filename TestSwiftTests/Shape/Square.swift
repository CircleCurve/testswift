//
//  Square.swift
//  TestSwift
//
//  Created by Patrick Ng on 5/3/2018.
//  Copyright © 2018 Gameone. All rights reserved.
//

import Foundation
class Square : NameShape {
    var sideLength : Double
    
    init (sideLength: Double, name: String) {
        self.sideLength = sideLength
        super.init(name:name)
        numberOfSides = 4
    }
    
    func area() -> Double {
        return sideLength * sideLength
    }
    
    override func simpleDescription() -> String {
        return "A square with side of length \(sideLength)."
    }
}
