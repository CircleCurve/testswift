//
//  TriangleAndSquare.swift
//  TestSwift
//
//  Created by Patrick Ng on 6/3/2018.
//  Copyright © 2018 Gameone. All rights reserved.
//

import Foundation
class TriangleAndSquare {
    var triangle : EquilateralTriangle {
        willSet {
            square.sideLength = newValue.sideLength
        }
    }
    
    var square : Square {
        willSet {
            triangle.sideLength = newValue.sideLength
        }
    }
    
    init(size : Double, name: String) {
        square = Square(sideLength: size, name: name)
        triangle = EquilateralTriangle(sideLength: size, name: name)
    }
}
