//
//  NameShape.swift
//  TestSwift
//
//  Created by Patrick Ng on 5/3/2018.
//  Copyright © 2018 Gameone. All rights reserved.
//

import Foundation
class NameShape {
    var numberOfSides : Int = 0
    var name : String
    
    init (name : String) {
        self.name = name
    }
    
    func simpleDescription()->String {
        return "A shape with \(numberOfSides) sides"
    }
}
