//
//  EquilateralTriangle.swift
//  TestSwift
//
//  Created by Patrick Ng on 6/3/2018.
//  Copyright © 2018 Gameone. All rights reserved.
//

import Foundation
class EquilateralTriangle : NameShape {
    var sideLength:Double = 0.0
    
    init (sideLength: Double, name: String) {
        self.sideLength = sideLength
        super.init(name: name)
        numberOfSides = 3
    }
    
    var perimeter : Double {
        get {
            return 3.0 * sideLength
        }
        
        set {
            sideLength = newValue / 3.0
        }
    }
    
    override func simpleDescription() -> String {
        return "An equilateral triangle with sides of length \(sideLength)."
    }
}
