//
//  Circle.swift
//  TestSwift
//
//  Created by Patrick Ng on 5/3/2018.
//  Copyright © 2018 Gameone. All rights reserved.
//

import Foundation
class Circle : NameShape {
    var radius : Double
    
    init (name: String, radius: Double){
        self.radius = radius
        super.init(name:name)
    }
    
    func area() -> Double {
        return radius * radius * Double.pi
    }
    
    override func simpleDescription() -> String {
        return "A cicle with radius \(radius)."
    }
}
