//
//  Shape.swift
//  TestSwift
//
//  Created by Patrick Ng on 5/3/2018.
//  Copyright © 2018 Gameone. All rights reserved.
//

import Foundation

class Shape {
    var numberOfSides = 0
    func simpleDescription () -> String {
        return "A shape with \(numberOfSides) sides"
    }
    
}
